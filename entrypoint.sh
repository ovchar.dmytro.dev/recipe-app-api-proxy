#!/bin/sh

# fail on any error
set -e

# populate the template
envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf

# start NGINX service
nginx -g "daemon off;"
